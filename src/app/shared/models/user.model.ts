export interface IUser {
  _id?: string;
  email?: string;
  password?: string;
  name?: string;
  surname?: string;
  phone?: string;
  activated?: string;
  activationKey?: string;
  activationExpires?: Date;
  resetKey?: string;
  resetDate?: Date;
}

export class User implements IUser {
  constructor(public _id?: string,
              public email?: string,
              public password?: string,
              public name?: string,
              public surname?: string,
              public phone?: string,
              public activated?: string,
              public activationKey?: string,
              public activationExpires?: Date,
              public resetKey?: string,
              public resetDate?: Date,
  )
  {
  }


}
