export interface IIp {
    as?: string;
    city?: string;
    country?: string;
    countryCode?: string;
    isp?: string;
    lat?: number;
    lon?: number;
    org?: string;
    query?: string;
    region?: string;
    regionName?: string;
    status?: string;
    timezone?: string;
    zip?: string;
}

export class Ip implements IIp {
    constructor(
        public as?: string,
        public city?: string,
        public country?: string,
        public countryCode?: string,
        public isp?: string,
        public lat?: number,
        public lon?: number,
        public org?: string,
        public query?: string,
        public region?: string,
        public regionName?: string,
        public status?: string,
        public timezone?: string,
        public zip?: string,
    ) {
    }
}
