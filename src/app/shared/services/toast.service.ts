import {ToastController} from '@ionic/angular';
import {Injectable} from '@angular/core';

@Injectable({
  providedIn: 'root'
})
export class ToastService {

  constructor(private toastController: ToastController) {}

  async presentToast(message: any, duration: any, position: any) {
    const toast = await this.toastController.create({
      message,
      duration,
      position
    });
    toast.present();
  }
}
