import {Injectable} from '@angular/core';
import {HttpClient} from '@angular/common/http';

import {BehaviorSubject} from 'rxjs';

import {environment} from '../../../environments/environment';
import {IUser, User} from '../../shared/models/user.model';
import {ToastService} from './toast.service';
import {IIp} from '../../shared/models/ip.model';
import {Router} from '@angular/router';
import {Storage} from '@ionic/storage';
import {Platform} from '@ionic/angular';
import {switchMap} from 'rxjs/operators';


const TOKEN_KEY = 'auth-token';

@Injectable({
  providedIn: 'root'
})
export class AuthService {
  authenticationState = new BehaviorSubject(false);

  private activeUser: IUser;
  private token: string;
  private userWhoIs: IIp;

  constructor(
    private storage: Storage,
    private plt: Platform,
    private http: HttpClient,
    private toastService: ToastService,
    private router: Router) {

    this.plt.ready().then(() => {
      this.checkToken();
    });
  }

  login(user: IUser) {
    return this.http.post<{ token: string; user: IUser }>(environment.url + '/login', user).pipe(
      switchMap(response => {
        const token = response.token;
        this.token = token;
        this.toastService.presentToast('Başarı İle Giriş Yapıldı.', 1000, 'top');
        this.activeUser = response.user;
        this.setUserWhoIs();
        this.storage.set('user', this.activeUser);
        return this.storage.set(TOKEN_KEY, this.token).then(res => {
          this.authenticationState.next(true);
        });
      })
    );
  }

  forgetPassword(email: any) {
    const url = environment.url + '/forget-password'
    return this.http.post(url, {email});
  }

  logout() {
    return this.storage.remove(TOKEN_KEY).then(() => {
      this.authenticationState.next(false);
      this.router.navigateByUrl('');
      this.toastService.presentToast('Başarı İle Çıkış Yapıldı.', 1000, 'top');
    });

  }

  isAuthenticated() {
    return this.authenticationState.value;

  }

  checkToken() {
    return this.storage.get(TOKEN_KEY).then(res => {
      if (res) {
        this.authenticationState.next(true);
      }
    });
  }

  private setUserWhoIs() {
    this.http
      .get<IIp>('http://ip-api.com/json/')
      .subscribe((data: IIp) => (this.userWhoIs = data));
  }

  getUserId() {
    return this.activeUser._id;
  }

  createUser(userParam: IUser) {
    const user = userParam;

    return this.http.post<IUser>(
      environment.url + '/register',
      user
    ).pipe(
      switchMap((res: any) => {
        if (res.result.email && res.result.password) {
          const userRes = new User();
          userRes.email = user.email;
          userRes.password = user.password;
          return this.login(userRes);
        }
      })
    );
  }

  getPayload(): Promise<any> {
    return this.storage.get(TOKEN_KEY);
  }

  public async getToken() {
    return await this.storage.get('auth-token');
  }

  public async getUserFromStorage() {
    return await this.storage.get('user');
  }
}
