import {NgModule} from '@angular/core';

import {IonicModule} from '@ionic/angular';

import {PagesRoutingModule} from './pages-routing.module';
import {HomePage} from "./home/home.page";
import {RegisterPage} from "./user/register/register.page";
import {LoginPage} from "./user/login/login.page";
import {CommonModule} from "@angular/common";
import {FormsModule, ReactiveFormsModule} from "@angular/forms";

@NgModule({
  declarations: [HomePage, RegisterPage, LoginPage],
  imports: [IonicModule,
    CommonModule,
    PagesRoutingModule,
    FormsModule,
    ReactiveFormsModule
  ]
})
export class PagesModule {
}
