import {Component, OnInit} from '@angular/core';
import {FormBuilder, FormGroup, Validators} from '@angular/forms';
import {Router} from '@angular/router';
import {User} from "../../../shared/models/user.model";
import {ToastService} from "../../../shared/services/toast.service";
import {AuthService} from "../../../shared/services/auth.service";
import { ConfirmedValidator } from '../../../shared/helpers/confirmed.validator';

@Component({
  selector: 'app-register',
  templateUrl: './register.page.html',
  styleUrls: ['./register.page.scss'],
})
export class RegisterPage implements OnInit {
  public registerForm: FormGroup;

  constructor(public authService: AuthService,
              private router: Router,
              private toastService: ToastService,
              private fb: FormBuilder) {
    this.initializeForm();
  }

  ngOnInit() {
    console.log("register form")
  }

  initializeForm() {
    this.registerForm = this.fb.group({
      name: ['', Validators.required],
      surname: ['', Validators.required],
      phone: ['', Validators.required],
      email: ['', Validators.compose([
        Validators.required,
        Validators.email
      ])],
      password: ['', Validators.compose([
        Validators.required,
        Validators.min(6)
      ])],
        confirm_password: ['', [Validators.required]]
    },
      {
        validator: ConfirmedValidator('password', 'confirm_password')
      }
  )
  }
  onClick() {
  }
 onRegister() {
     const email = this.registerForm.value.email;
     const password = this.registerForm.value.password;
     const name = this.registerForm.value.name;
     const surname = this.registerForm.value.surname;
     const phone = this.registerForm.value.phone
     const user = new User();
     user.email = email;
     user.password = password;
     user.name = name;
     user.phone = phone;
     user.surname = surname;
     // this.authService.createUser(user)
    //    .subscribe(status => {
     //     if (this.authService.isAuthenticated()) {
     //       this.router.navigateByUrl('home');
     //     }
     //   }, error => {
     //     this.toastService.presentToast(error.error, 3000, 'top');
     //   });
   console.log(this.registerForm.value.password)
  }
}

